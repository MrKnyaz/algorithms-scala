package AlgorithmsScala;

import junit.framework._;
import Assert._
import net.aknyazev.datastructures.MinPriorityQueue
;

object AppTest {
    def suite: Test = {
        val suite = new TestSuite(classOf[AppTest]);
        suite
    }

    def main(args : Array[String]) {
        junit.textui.TestRunner.run(suite);
    }
}

/**
 * Unit test for simple App.
 */
class AppTest extends TestCase("app") {

    /**
     * Rigourous Tests :-)
     */
    //def testOK() = assertTrue(true);
    //def testKO() = assertTrue(false);
    
    def testMinPQ() {
      val pq  = new MinPriorityQueue[java.lang.Long]()
      for (i <- 1 to 30) {
        pq.add(Math.round(Math.random()*1000))
      }
      pq.write()
      for (i <- 1 to 30) {
        println(pq.pop())
      }

      //println(Math.round(Math.random()*1000))
      /*pq.add(10)
      pq.add(9)
      pq.add(8)
      pq.add(7)*/
    }
}
