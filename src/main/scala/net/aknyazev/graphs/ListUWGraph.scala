package net.aknyazev.graphs

import scala.collection.mutable.LinkedList

/**
 * Author: MrKnyaz
 * Date: 07.08.13
 */
class ListUWGraph(vertices: Int) {

  class Edge(v: Int, w: Int, weight: Int) {
    def getAnother(vert: Int) =  if (vert == v) w else v;
  }
  val adj: Array[LinkedList[Edge]] = new Array[LinkedList[Edge]](vertices);
  def getAdjacent(vert: Int) = adj(vert)
}
