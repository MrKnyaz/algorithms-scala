package net.aknyazev.datastructures

import scala.collection.mutable.ArrayBuffer

/**
 * Author: MrKnyaz
 * Date: 18.08.13
 */
class MinPriorityQueue[T <: Comparable[T]] {
  private val data = new ArrayBuffer[T]()
  private var size: Int = 0

  def add(value: T) {
    data += (value)
    size += 1
    up(size - 1)
    //write
  }

  def pop(): T = {
    if (size > 0) {
      val result = data(0)
      data(0) = data(size-1)
      down(0)
      return result
    } else return null.asInstanceOf[T]
  }

  private def down(i: Int) {
    var parent = i
    var child = 2 * i + 1
    while (child < size) {
      if (child + 1 < size && data(child + 1).compareTo(data(child)) < 0) child += 1
      if (data(child).compareTo(data(parent)) < 0) {
        exch(parent, child)
        parent = child;
        child = 2 * child + 1;
      } else child = size
    }
  }

  private def up(i: Int) {
    var child = i
    var parent = (i - 1) / 2
    while (child > 0) {
      if (data(child).compareTo(data(parent)) < 0) {
        exch(child, parent)
        child = parent
        parent = (child - 1) / 2
      } else child = 0
    }
  }

  private def exch(i: Int, j: Int) {
    val tmp = data(i)
    data(i) = data(j)
    data(j) = tmp
  }

  def write() {
    println(data)
  }
}
